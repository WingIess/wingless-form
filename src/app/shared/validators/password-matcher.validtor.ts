import { AbstractControl, ValidatorFn } from '@angular/forms';

export function matchingWithField(matchingFieldName: string): ValidatorFn {
  return (passwordConfirmation: AbstractControl) => {
    const password = passwordConfirmation.parent?.get(matchingFieldName)?.value;
    return password === passwordConfirmation.value
      ? null
      : { confirmPassword: true };
  };
}
