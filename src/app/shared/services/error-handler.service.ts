import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { errorMessages } from '../constants/error-messages.enum';

@Injectable({ providedIn: 'root' })
export class ErrorHandlerService {
  constructor(private snacBar: MatSnackBar) {}

  openErrorSnacBar(error: HttpErrorResponse) {
    const rawErrorMessage: keyof typeof errorMessages =
      error?.error?.error?.message;
    if (error?.error?.error?.message) {
      const errorMessage = errorMessages[rawErrorMessage];
      this.snacBar.open(errorMessage, 'dismiss', {
        duration: 3000,
      });
    }
  }
}
