import { HttpClient, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { User } from '../models/user.model';

export interface AuthenticateResponse {
  idToken: string;
  email: string;
  refreshToken: string;
  expiresIn: string;
  localId: string;
  registered?: boolean;
}

export interface SessionUser {
  email: string;
  id: string;
  _token: string;
  _tokenExpirationDate: string;
}

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
  user = new BehaviorSubject<User | null>(null);
  loggedIn = false;

  constructor(private http: HttpClient, private router: Router) {}

  signUpUser(email: string, password: string) {
    const signUpObservable = this.http.post<AuthenticateResponse>('signUp', {
      email: email,
      password: password,
      returnSecureToken: true,
    });

    signUpObservable.subscribe((responseData) => {
      this.handleLoginUser(
        responseData.email,
        responseData.localId,
        responseData.idToken,
        +responseData.expiresIn
      );

      this.loggedIn = true;
    });

    return signUpObservable;
  }

  login(email: string, password: string): Observable<AuthenticateResponse> {
    const responseObserver = this.http.post<AuthenticateResponse>(
      'signInWithPassword',
      {
        email: email,
        password: password,
        returnSecureToken: true,
      }
    );

    responseObserver.subscribe((responseData) => {
      this.handleLoginUser(
        responseData.email,
        responseData.localId,
        responseData.idToken,
        +responseData.expiresIn
      );

      this.loggedIn = true;
    });

    return responseObserver;
  }

  private handleLoginUser(
    email: string,
    userID: string,
    token: string,
    expiresIn: number
  ) {
    const expirationDate = new Date(new Date().getTime() + expiresIn * 1000);
    const user = new User(email, userID, token, expirationDate);

    sessionStorage.setItem('userData', JSON.stringify(user));

    this.user.next(user);
    this.router.navigate(['forms']);
  }

  autoLogin() {
    const userData = sessionStorage.getItem('userData');
    if (userData) {
      const parsedUser: SessionUser = JSON.parse(userData as string);
      const user = new User(
        parsedUser.email,
        parsedUser.id,
        parsedUser._token,
        new Date(parsedUser._tokenExpirationDate)
      );

      if (user.token) {
        this.loggedIn = true;
        this.user.next(user);
      }
    }
  }
}
