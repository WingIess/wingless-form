import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class LoadingService {
  loadingMap = new BehaviorSubject<string[]>([]);

  constructor() {}

  checkItemLoading(item: string): boolean {
    return this.loadingMap.getValue().includes(item);
  }

  pushItemLoading(item: string): void {
    const newLoadingMap = [...this.loadingMap.getValue(), item];
    this.loadingMap.next(newLoadingMap);
  }

  removeItemFormLoading(item: string): void {
    const itemIndex = this.loadingMap.getValue().findIndex((it) => it === item);
    const newLoadingMap = this.loadingMap.getValue();
    newLoadingMap.splice(itemIndex, 1);
    this.loadingMap.next(newLoadingMap);
  }
}
