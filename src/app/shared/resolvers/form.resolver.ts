import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { FormsService } from 'src/app/forms.service';
import { FormModel } from 'src/app/home/form-card/form.model';

@Injectable({ providedIn: 'root' })
export class FormResolver implements Resolve<FormModel> {
  constructor(private formService: FormsService) {}

  resolve(route: ActivatedRouteSnapshot) {
    const formObservable = this.formService.fetchForm(route.params.id);
    return formObservable;
  }
}
