import {
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { generateDefaultUrl } from '../../shared/utility/helper-functions';
@Injectable()
export class DefaultUrlInterceptorService implements HttpInterceptor {
  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const url = generateDefaultUrl(req.url);

    if (!environment.production) {
      console.log('API call: ', url);
    }

    const modifedReq = req.clone({
      url,
    });

    return next.handle(modifedReq);
  }
}
