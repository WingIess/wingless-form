import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { ErrorHandlerService } from '../services/error-handler.service';
import { LoadingService } from '../services/loading.service';

@Injectable()
export class LoadingInterceptorService implements HttpInterceptor {
  constructor(
    private loadingService: LoadingService,
    private errorHandlerService: ErrorHandlerService
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const httpObservable = next.handle(req);

    const loadingKey = `${req.method}-${req.urlWithParams}`;

    this.loadingService.pushItemLoading(loadingKey);

    return httpObservable.pipe<HttpEvent<any>, any>(
      map<HttpEvent<any>, any>((evt: HttpEvent<any>) => {
        this.loadingService.removeItemFormLoading(loadingKey);
        return evt;
      }),
      catchError((err) => {
        this.errorHandlerService.openErrorSnacBar(err);
        this.loadingService.removeItemFormLoading(loadingKey);
        return throwError('error');
      })
    );
  }
}
