import { environment } from 'src/environments/environment';

export const generateDefaultUrl = (url: string): string => {
  if (url[0] === '/') {
    return environment.db_url + url + '.json';
  } else {
    return environment.auth_url + url + '?key=' + environment.api_key;
  }
};
