import { FormInputModel } from './formInput.model';

export class GroupModel {
  constructor(
    public groupTitle = '',
    public errorMessage = '',
    public inputForms: FormInputModel[] = []
  ) {}
}
