export class FormInputModel {
  constructor(
    public inputType: 'text' | 'radio' | 'select' | 'checkbox',
    public label: string,
    public value?: string | boolean,
    public validators?: string[],
    public errorMessage?: string,
    public options?: string[],
    public showLabel?: boolean
  ) {}
}
