export class UserForm {
  constructor(
    public id?: string,
    public title?: string,
    public description?: string,
    public date?: Date
  ) {}
}
