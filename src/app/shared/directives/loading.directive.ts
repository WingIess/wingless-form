import {
  Directive,
  HostBinding,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { LoadingService } from '../services/loading.service';
import { generateDefaultUrl } from '../utility/helper-functions';

@Directive({
  selector: '[appLoading]',
})
export class LoadingDirective implements OnInit, OnDestroy {
  @Input() appLoading: { req: string; url: string } = { req: '', url: '' };
  @Input() displayTo = 'block';
  @HostBinding('style.display') display = 'none';
  loadingMapSubscription = new Subscription();

  constructor(private loadingService: LoadingService) {}

  ngOnInit() {
    this.loadingMapSubscription = this.loadingService.loadingMap.subscribe(
      (loadingUrls) => {
        this.handleDisplay(loadingUrls);
      }
    );
  }

  ngOnDestroy() {
    this.loadingMapSubscription.unsubscribe();
  }

  retriveUrl(url: string, reqType: string): string {
    return `${reqType}-${generateDefaultUrl(url)}`;
  }

  handleDisplay(loadingUrls: string[]) {
    if (
      loadingUrls.includes(
        this.retriveUrl(this.appLoading.url, this.appLoading.req)
      )
    ) {
      this.display = this.displayTo;
    } else {
      this.display = 'none';
    }
  }
}
