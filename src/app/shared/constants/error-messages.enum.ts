export enum errorMessages {
  // login error messages
  EMAIL_NOT_FOUND = 'Email not found',
  INVALID_PASSWORD = 'Invalid password',
  USER_DISABLED = 'User disabled',
  'TOO_MANY_ATTEMPTS_TRY_LATER : Access to this account has been temporarily disabled due to many failed login attempts. You can immediately restore it by resetting your password or you can try again later.' = 'Too many failed attempts, please try later',
  MISSING_PASSWORD = 'Missing password',

  // registration error messages
  EMAIL_EXISTS = 'Email already registered',
  OPERATION_NOT_ALLOWED = 'Not permitted action',

  undefined = 'Unknown error',
}
