import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { AuthenticationService } from '../shared/services/authentication.service';

import { matchingWithField } from '../shared/validators/password-matcher.validtor';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {
  userData: FormGroup = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [
      Validators.required,
      Validators.minLength(6),
    ]),
    confirmPassword: new FormControl('', [
      Validators.required,
      matchingWithField('password'),
    ]),
  });
  loading = false;

  constructor(private auth: AuthenticationService, private router: Router) {}

  ngOnInit(): void {}

  getControl(controlName: string): FormControl {
    return this.userData.get(controlName) as FormControl;
  }

  handleRegistration() {
    if (this.userData.valid) {
      this.loading = true;
      this.auth
        .signUpUser(
          this.getControl('email')?.value,
          this.getControl('password')?.value
        )
        .pipe(
          map(() => {
            this.handleSignUpSuccess();
          })
        );
    }
  }

  handleSignUpSuccess() {
    this.loading = false;
    this.router.navigate(['login']);
  }
}
