import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationService } from '../shared/services/authentication.service';

@Component({
  selector: 'app-authentication-page',
  templateUrl: './authentication-page.component.html',
  styleUrls: ['./authentication-page.component.scss'],
})
export class AuthenticationPageComponent implements OnInit {
  userData = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required]),
    password: new FormControl('', [Validators.minLength(6)]),
  });
  loading = false;

  constructor(private authService: AuthenticationService) {}

  ngOnInit(): void {}

  getControl(controlName: string): FormControl {
    return this.userData.get(controlName) as FormControl;
  }

  onHandleLogin() {
    if (this.userData.valid) {
      this.authService.login(
        this.getControl('email').value,
        this.getControl('password').value
      );
    }
  }
}
