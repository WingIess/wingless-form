import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormEditorComponent } from './form-editor/form-editor.component';
import { FormCreatorComponent } from './form-creator/form-creator.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { HeaderComponent } from './header/header.component';
import { FormCardComponent } from './home/form-card/form-card.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormInputComponent } from './form-editor/form-input/form-input.component';
import { SimpleComponent } from './form-editor/section/simple/simple.component';
import { StepperComponent } from './form-editor/section/stepper/stepper.component';
import { TableSectionComponent } from './form-editor/section/table/table.component';
import { SectionComponent } from './form-editor/section/section.component';
import { FormEditorResultComponent } from './form-editor-result/form-editor-result.component';
import { AuthenticationPageComponent } from './authentication-page/authentication-page.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationInterceptorService } from './shared/interceptors/authentication-interceptor.service';
import { DefaultUrlInterceptorService } from './shared/interceptors/default-url-interceptor.service';
import { LoadingDirective } from './shared/directives/loading.directive';
import { LoadingInterceptorService } from './shared/interceptors/loading-interceptor.service';
import { SpinnerComponent } from './shared/components/spinner/spinner.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormEditorComponent,
    FormCreatorComponent,
    NotFoundPageComponent,
    HeaderComponent,
    FormCardComponent,
    FormInputComponent,
    SimpleComponent,
    StepperComponent,
    TableSectionComponent,
    SectionComponent,
    FormEditorResultComponent,
    AuthenticationPageComponent,
    SignUpComponent,
    LoadingDirective,
    SpinnerComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    FormsModule,
    MatStepperModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    MatSelectModule,
    MatRadioModule,
    HttpClientModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: DefaultUrlInterceptorService,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoadingInterceptorService,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
