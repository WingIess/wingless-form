import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { inputTypeEnum } from './inputType.enum';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.scss'],
})
export class FormInputComponent implements OnInit {
  @Input() inputType = 'text';
  @Input() label!: string;
  @Input() showLabel?: boolean;
  @Input() inputFormControl!: FormControl;
  @Input() errorMessage?: string;
  @Input() options?: string[];

  inputTypeEnum = inputTypeEnum;

  constructor() {}

  ngOnInit(): void {}
}
