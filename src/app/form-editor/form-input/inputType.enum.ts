export enum inputTypeEnum {
  TEXT = 'text',
  CHECKBOX = 'checkbox',
  SELECT = 'select',
  RADIO = 'radio',
}
