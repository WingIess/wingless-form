import { GroupModel } from 'src/app/shared/models/group.model';

export class SimpleSectionModel {
  constructor(
    public sectionTitle = '',
    public sectionType: 'simple' = 'simple',
    public groups: GroupModel[] = [],
    public showHeader: boolean = false
  ) {}
}
