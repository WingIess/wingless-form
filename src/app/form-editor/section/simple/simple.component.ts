import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { GroupModel } from 'src/app/shared/models/group.model';

@Component({
  selector: 'app-simple',
  templateUrl: './simple.component.html',
  styleUrls: ['./simple.component.scss'],
})
export class SimpleComponent implements OnInit {
  @Input() groups: GroupModel[] = [];
  @Input() stepperControl: AbstractControl | null = null;

  constructor() {}

  getStepperFormGroup(groupTitle: string): FormGroup {
    return this.stepperControl?.get(groupTitle) as FormGroup;
  }

  getInputFormControl(groupTitle: string, inputLabel: string): FormControl {
    return this.getStepperFormGroup(groupTitle).get(inputLabel) as FormControl;
  }

  ngOnInit(): void {}
}
