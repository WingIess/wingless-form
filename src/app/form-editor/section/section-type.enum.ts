export enum SectionType {
    SIMPLE = 'simple',
    STEPPER = 'stepper',
    TABLE = 'table',
}
