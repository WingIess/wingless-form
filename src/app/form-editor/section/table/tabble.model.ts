import { GroupModel } from 'src/app/shared/models/group.model';

export class TableModel {
  constructor(
    public sectionTitle = '',
    public sectionType: 'table' = 'table',
    public row: string[] = [],
    public groups: GroupModel[] = [],
    public showHeader: boolean = false
  ) {}
}
