import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { GroupModel } from 'src/app/shared/models/group.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableSectionComponent implements OnInit {
  @Input() groups: GroupModel[] = [];
  @Input() stepperControl: AbstractControl | null = null;

  constructor() {}

  ngOnInit(): void {}
}
