import { GroupModel } from 'src/app/shared/models/group.model';

type ArrayTwoOrMore<T> = {
  0: T;
  1: T;
} & Array<T>;

// TODO: implement minimum 2 length array format on groups
export class StepperModel {
  constructor(
    public sectionTitle = '',
    public sectionType: 'stepper' = 'stepper',
    public groups: GroupModel[] = [],
    public showHeader: boolean = false
  ) {}
}
