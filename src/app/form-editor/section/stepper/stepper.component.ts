import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormControl, FormGroup } from '@angular/forms';
import { GroupModel } from 'src/app/shared/models/group.model';

@Component({
  selector: 'app-stepper',
  templateUrl: './stepper.component.html',
  styleUrls: ['./stepper.component.scss'],
})
export class StepperComponent implements OnInit {
  @Input() groups: GroupModel[] = [];
  @Input() sectionGroupControl: AbstractControl | null = null;

  constructor() {}

  getStepperFormGroup(groupTitle: string): FormGroup {
    return this.sectionGroupControl?.get(groupTitle) as FormGroup;
  }

  getInputFormControl(groupTitle: string, inputLabel: string): FormControl {
    return this.getStepperFormGroup(groupTitle).get(inputLabel) as FormControl;
  }

  canSubmit(): boolean {
    return (this.sectionGroupControl?.valid &&
      this.sectionGroupControl.dirty &&
      this.sectionGroupControl.touched) as boolean;
  }

  onSubmitHandler(): void {}

  ngOnInit(): void {}
}
