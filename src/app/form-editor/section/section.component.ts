import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { SectionType } from './section-type.enum';
import { SimpleSectionModel } from './simple/simple.model';
import { StepperModel } from './stepper/stepper.model';
import { TableModel } from './table/tabble.model';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.scss'],
})
export class SectionComponent implements OnInit {
  @Input() section!: SimpleSectionModel | StepperModel | TableModel;
  @Input() sectionControl: AbstractControl | null = null;
  sectionTypeEnum = SectionType;

  constructor() {}

  // getFormGroup(groupTitle: string): FormGroup {
  //   return this.sectionGroup?.get(groupTitle) as FormGroup;
  // }

  ngOnInit(): void {}
}
