import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FormsService } from '../forms.service';
import { FormModel } from '../home/form-card/form.model';
import { FormInputModel } from '../shared/models/formInput.model';
import { reqExpValidator } from '../shared/validators/reqexp.validator';
import { StepperModel } from './section/stepper/stepper.model';

interface ValidatorTypeToValidatorModel {
  [key: string]: Validators;
}
const ValidatorTypeToValidator: ValidatorTypeToValidatorModel = {
  ['required']: Validators.required,
  ['requiredTrue']: Validators.requiredTrue,
  ['email']: Validators.email,
  ['ip']: reqExpValidator(new RegExp('^(?:[0-9]{1,3}.){3}[0-9]{1,3}$')),
};

interface GroupInterface {
  [key: string]: FormGroup;
}

interface FormInputsInterface {
  [key: string]: FormControl;
}

interface FormInputInterface {
  [key: string]: string;
}

@Component({
  selector: 'app-form-editor',
  templateUrl: './form-editor.component.html',
  styleUrls: ['./form-editor.component.scss'],
  providers: [
    {
      provide: STEPPER_GLOBAL_OPTIONS,
      useValue: { showError: true },
    },
  ],
})
export class FormEditorComponent implements OnInit {
  form!: FormModel;
  formInputGroups = new FormGroup({});

  constructor(
    private route: ActivatedRoute,
    private formsService: FormsService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.initializateForm();
    this.generateFormFromData();
  }

  initializateForm(): void {
    this.form = this.route.snapshot.data.form as FormModel;
  }

  // TODO: make this work and use it in generateFormFromData
  // populateGroupWithItems(
  //   items: Array<FormInputModel>,
  //   key: string,
  //   valueFormater: (item: FormInputModel) => FormGroup | FormControl
  // ): object {
  //   const createdGroup: GroupInterface | FormInputsInterface = {};
  //   items.forEach((item) => {
  //     createdGroup[item[key]] = valueFormater(item);
  //   });
  //   return createdGroup;
  // }

  generateFormFromData(): void {
    const sections: GroupInterface = {};
    this.form.sections.forEach((section) => {
      const groups: GroupInterface = {};
      section.groups.forEach((group) => {
        const inputs: FormInputsInterface = {};
        group.inputForms.forEach((input) => {
          inputs[input.label] = this.createInput(input);
        });
        groups[group.groupTitle] = new FormGroup(inputs);
      });
      sections[section.sectionTitle] = new FormGroup(groups);
    });
    this.formInputGroups = new FormGroup(sections);
  }

  createInput(input: FormInputModel): FormControl {
    return input.validators
      ? new FormControl(
          input.value,
          ...input.validators.map(
            (validatorStr) => ValidatorTypeToValidator[validatorStr]
          )
        )
      : new FormControl(input.value);
  }

  getSectionGroupForm(index: number): FormGroup {
    return this.formInputGroups.get(
      this.form.sections[index].sectionTitle
    ) as FormGroup;
  }

  modifyForm(): void {
    const newValues = this.formInputGroups.value;
    // const newForm = {
    //   ...this.form,
    //   sections: Object.keys(newValues).map((sectionTitle) => {
    //     const section = this.form.sections.find(
    //       (section) => section.sectionTitle === sectionTitle
    //     );
    //     return {
    //       ...section,
    //       groups: section?.groups.map((group) => {
    //         return {
    //           ...group,
    //           inputForms: group.inputForms.map((inputForm) => {
    //             const newValue =
    //               newValues[section.sectionTitle][group.groupTitle][
    //                 inputForm.label
    //               ];
    //             return {
    //               ...inputForm,
    //               value: newValue,
    //             };
    //           }),
    //         };
    //       }),
    //     } as StepperModel;
    //   }),
    // };

    if (this.form.id && this.form.belongToUser) {
      this.formsService.answerForm(
        this.form.id,
        newValues,
        this.form.belongToUser
      );
      // this.router.navigate(['form-editor-result', this.form.id]);
    }
  }
}
