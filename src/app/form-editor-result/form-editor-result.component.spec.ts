import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormEditorResultComponent } from './form-editor-result.component';

describe('FormEditorResultComponent', () => {
  let component: FormEditorResultComponent;
  let fixture: ComponentFixture<FormEditorResultComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormEditorResultComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormEditorResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
