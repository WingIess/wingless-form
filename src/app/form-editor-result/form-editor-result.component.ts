import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormsService } from '../forms.service';
import { FormModel } from '../home/form-card/form.model';

@Component({
  selector: 'app-form-editor-result',
  templateUrl: './form-editor-result.component.html',
  styleUrls: ['./form-editor-result.component.scss'],
})
export class FormEditorResultComponent implements OnInit {
  form?: FormModel;
  constructor(
    private route: ActivatedRoute,
    private formService: FormsService
  ) {}

  ngOnInit(): void {
    const foundForm = this.formService.getFormById(
      this.route.snapshot.params.id
    );
    if (foundForm) {
      this.form = foundForm;
    }
  }
}
