import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormModel } from './home/form-card/form.model';
import { UserForm } from './shared/models/userForms.model';
import { AuthenticationService } from './shared/services/authentication.service';

@Injectable({
  providedIn: 'root',
})
export class FormsService {
  userFormsSubject = new BehaviorSubject<UserForm[]>([]);
  forms: FormModel[] = [];
  userFormsLoaded = false;

  constructor(
    private authService: AuthenticationService,
    private http: HttpClient
  ) {}

  get userForms() {
    return this.userFormsSubject.getValue();
  }

  fetchUserForms(): Observable<UserForm[]> {
    const userFormsObservable = this.http.get<UserForm[]>(
      `/users/${this.authService.user.getValue()?.id}/forms`
    );

    userFormsObservable.subscribe((userForms) => {
      this.userFormsLoaded = true;
      this.userFormsSubject.next(userForms);
    });

    return userFormsObservable;
  }

  fetchForm(id: string): Observable<FormModel> {
    const formModelObservable = this.http.get<FormModel>('/forms/' + id);
    formModelObservable.subscribe((form) => {
      this.forms.push(form);
    });
    return formModelObservable;
  }

  answerForm(formId: string, answer: object, belongToUserId: string) {
    const formAnswerObservable = this.http.post(
      `/users/${belongToUserId}/results/${formId}`,
      { ...answer }
    );

    formAnswerObservable.subscribe((responseData) => {
      console.log(responseData);
    });

    return formAnswerObservable;
  }

  private findFormIndexById(id: string | null | undefined): number {
    return this.forms.findIndex((it) => id === it.id);
  }

  getFormById(id: string | null): FormModel {
    return this.forms[this.findFormIndexById(id)];
  }

  createForm(form: FormModel): void {
    this.forms.push(form);
  }

  modifyForm(form: FormModel): void {
    const modifedFormIndex = this.findFormIndexById(form.id);
    this.forms[modifedFormIndex] = { ...form };
  }

  deleteForm(form: FormModel): void {
    const modifedFormIndex = this.findFormIndexById(form.id);
    this.forms.splice(modifedFormIndex, 1);
  }
}
