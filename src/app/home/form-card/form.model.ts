import { SimpleSectionModel } from 'src/app/form-editor/section/simple/simple.model';
import { StepperModel } from 'src/app/form-editor/section/stepper/stepper.model';
import { TableModel } from 'src/app/form-editor/section/table/tabble.model';

export class FormModel {
  constructor(
    public belongToUser: string | null,
    public id: string | null = null,
    public title = '',
    public description = '',
    public date = new Date(),
    public sections: Array<SimpleSectionModel | StepperModel | TableModel> = []
  ) {}
}
