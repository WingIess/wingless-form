export class SectionModel {
  constructor(
    public sectionTitle = '',
    public sectionType: 'simple' | 'stepper' | 'table' = 'simple'
  ) {}
}
