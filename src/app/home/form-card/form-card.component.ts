import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserForm } from 'src/app/shared/models/userForms.model';

@Component({
  selector: 'app-form-card',
  templateUrl: './form-card.component.html',
  styleUrls: ['./form-card.component.scss'],
})
export class FormCardComponent implements OnInit, OnDestroy {
  @Input() form = new UserForm();
  @Input() addCard = false;
  loading = false;

  constructor(private router: Router) {}

  ngOnInit(): void {}

  ngOnDestroy() {
    this.loading = false;
  }

  onCardClicked() {
    if (this.addCard) {
      this.router.navigate(['create']);
    } else {
      this.loading = true;
      this.router.navigate(['form', this.form.id]);
    }
  }
}
