import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { FormsService } from '../forms.service';
import { UserForm } from '../shared/models/userForms.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  forms: UserForm[] = [];
  formsSubscription = new Subscription();

  constructor(private formsService: FormsService) {}

  ngOnInit(): void {
    if (!this.formsService.userFormsLoaded) {
      this.formsService.fetchUserForms();
    }

    this.formsSubscription = this.formsService.userFormsSubject.subscribe(
      (userForms) => {
        this.forms = userForms;
      }
    );
  }

  ngOnDestroy() {
    this.formsSubscription.unsubscribe();
  }
}
