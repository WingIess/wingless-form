import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthenticationPageComponent } from './authentication-page/authentication-page.component';
import { FormCreatorComponent } from './form-creator/form-creator.component';
import { FormEditorResultComponent } from './form-editor-result/form-editor-result.component';
import { FormEditorComponent } from './form-editor/form-editor.component';
import { HomeComponent } from './home/home.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { AlreadyLoggedInGuard } from './shared/guards/already-logged-in.guard';
import { AuthenticatedOnlyGuard } from './shared/guards/authenticated-only.guard';
import { FormResolver } from './shared/resolvers/form.resolver';
import { SignUpComponent } from './sign-up/sign-up.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login',
    component: AuthenticationPageComponent,
    canActivate: [AlreadyLoggedInGuard],
  },
  {
    path: 'sign-up',
    component: SignUpComponent,
    canActivate: [AlreadyLoggedInGuard],
  },
  {
    path: 'forms',
    component: HomeComponent,
    canActivate: [AuthenticatedOnlyGuard],
  },
  {
    path: 'create',
    component: FormCreatorComponent,
    canActivate: [AuthenticatedOnlyGuard],
  },
  {
    path: 'form/:id',
    component: FormEditorComponent,
    resolve: {
      form: FormResolver,
    },
  },
  { path: 'form-editor-result/:id', component: FormEditorResultComponent },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
